<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <table border="1" cellspacing="0">
        <?php foreach ($films as $filmItem): ?>
        <tr>
            <td><?php echo $filmItem['title_film']; ?></td>
            <td><?php echo $filmItem['year']; ?></td>
            <td><?php echo Films::getTagsListFilms($filmItem['id']) ?></td>
            <td><a href="/film/edit/<?php echo $filmItem['id']; ?>">изменить</a></td>
            <td><a href="/film/del/<?php echo $filmItem['id']; ?>">удалить</a></td>
        </tr>
        <?php endforeach; ?>
        <tr>
            <td colspan="5" style="text-align: center;"><a href="film/add">Добавить фильм</a></td>
        </tr>
    </table>
</body>
</html>