<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<form action="" method="POST">
    <p>Название фильма:</p>
    <input type="text" name="title" value="<?php echo $filmItem['title_film']; ?>"><br/>
    <p>Год выпуска:</p>
    <input type="text" name="year" value="<?php echo $filmItem['year']; ?>"><br/>
    <p>Жанры кино, через запятую:</p>
    <input type="text" name="tags" value="<?php echo Films::getTagsListFilmsEdit($filmItem['id']); ?>"><br/><br/>
    <input type="submit" name="submit" value="Изменить">
</form>
<a href="/">обратно</a>
</body>
</html>