<?php

return array(
    //Действия с фильмами
    'film/del/([0-9]+)' => 'film/del/$1',
    'film/del_tag/([0-9]+)/([0-9]+)' => 'film/delTag/$1/$2',
    'film/edit/([0-9]+)' => 'film/edit/$1',
    'film/add' => 'film/add',
    // Главная страница
    'index.php' => 'site/index', // actionIndex в SiteController
    '' => 'site/index', // actionIndex в SiteController
);
