<?php

class Films
{
    /*
     * Возвращает теги по id фильмов
     */
    public static function getTagsListFilms($id)
    {
        $db = Db::getConnection();

        $sql = "SELECT films.*, tags.*
                FROM distribution
                INNER JOIN films ON films.id = distribution.id_film
                INNER JOIN tags ON tags.id = distribution.id_tag
                WHERE films.id = :id
                ORDER BY films.title_film ASC";
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->execute();

        while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
           echo $row['title_tag'] . ' ' . '<a href="/film/del_tag/' . $id . '/' . $row['id']. '">x</a>' . ' ';
        }
    }

    /*
     * Возвращает теги по id фильмов на странице редактирования
     */
    public static function getTagsListFilmsEdit($id)
    {
        $db = Db::getConnection();

        $sql = "SELECT films.*, tags.*
                FROM distribution
                INNER JOIN films ON films.id = distribution.id_film
                INNER JOIN tags ON tags.id = distribution.id_tag
                WHERE films.id = :id
                ORDER BY films.title_film ASC";
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->execute();

        while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
            echo $row['title_tag'] . ' ';
        }
    }

    /*
     * возвращаем список фильмов
     */
    public static function getFilmslist()
    {
        $db = Db::getConnection();
        $sql = 'SELECT * FROM films ORDER BY title_film ASC';
        $result = $db->query($sql);

        $i = 0;
        $films = array();
        while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
            $films[$i]['id'] = $row['id'];
            $films[$i]['title_film'] = $row['title_film'];
            $films[$i]['year'] = $row['year'];
            $i++;
        }
        return $films;
    }

    /*
     * получить данные об одном фильме
     */
    public static function getFilmByID($id)
    {
        $db = Db::getConnection();
        $sql = 'SELECT * FROM `films` WHERE id = :id';
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->execute();
        return $result->fetch(PDO::FETCH_ASSOC);
    }

    /*
     * Добавление фильма
     */
    public static function addFilm($titleFilm, $yearFilm)
    {
        $db = Db::getConnection();

        if ($titleFilm !='' AND $yearFilm !='') {
            $sql = 'INSERT INTO films(title_film, year)
                    VALUES (:title_film, :year_film)';
            $result = $db->prepare($sql);
            $result->bindParam(':title_film', $titleFilm, PDO::PARAM_STR);
            $result->bindParam(':year_film', $yearFilm, PDO::PARAM_INT);
            if ($result->execute()) {
                return $db->lastInsertId();
            }
            return 0;
        }
    }

    /*
     * получение списка тегов
     */
    public static function getTagsList()
    {
        $db = Db::getConnection();

        $sql = 'SELECT title_tag FROM `tags`';
        $result = $db->query($sql);

        $tagList = array();
        while ($row = $result->fetch()) {
            $tagList[] = $row['title_tag'];
        }
        return $tagList;
    }
    /*
     * добавление тегов
     */
    public static function addTags($tagsFilm)
    {
        $db = Db::getConnection();
        $tagList = self::getTagsList();
        /*
         * проверяем теги введенные в форме с тегами в БД, если совпадений нет, то добавляем тег в БД
         */
        foreach ($tagsFilm as $tagItem) {
            if (!in_array($tagItem, $tagList) AND $tagItem != '') {
                $sql = 'INSERT INTO tags(title_tag)
                        VALUES (:title_tag)';
                $result = $db->prepare($sql);
                $result->bindParam(':title_tag', $tagItem, PDO::PARAM_STR);
                $result->execute();
            }
        }
    }

    /*
     * получение ID тегов указаных при добавлении фильма
     */
    public static function getIdTagCreateFilm($tagsFilm)
    {
        $db = Db::getConnection();
        $tagList = self::getTagsList();
        $idTags = array();

        foreach ($tagsFilm as $tag) {
            if(in_array($tag, $tagList)) {
                $sql = 'SELECT id FROM tags WHERE title_tag = :title_tag';
                $result = $db->prepare($sql);
                $result->bindParam(':title_tag', $tag, PDO::PARAM_STR);
                $result->execute();
                $idTags[] = $result->fetch(PDO::FETCH_COLUMN);
            }
        }
        return $idTags;
    }

    /*
     * добавление связи между id фильма и id тегов в таблице distribution
     */
    public static function addLink($id, $idTags)
    {
        $db = Db::getConnection();
        foreach ($idTags as $idTagItem) {
            $sql = 'INSERT INTO distribution(id_film, id_tag) VALUES (:id_film, :id_tag)';
            $result = $db->prepare($sql);
            $result->bindParam(':id_film', $id, PDO::PARAM_INT);
            $result->bindParam(':id_tag', $idTagItem, PDO::PARAM_INT);
            $result->execute();
        }
    }

    /*
     * обновление фильма в фильмотеке
     */
    public static function updateFilm($titleFilm, $yearFilm, $id)
    {
        $db = Db::getConnection();
        $sql = 'UPDATE films 
                SET title_film = :title_film, year = :year  
                WHERE id = :id';
        $result = $db->prepare($sql);
        $result->bindParam(':title_film', $titleFilm, PDO::PARAM_STR);
        $result->bindParam(':year', $yearFilm, PDO::PARAM_STR);
        $result->bindParam(':id', $id, PDO::PARAM_STR);
        $result->execute();
    }

    /*
     * удаление связи между фильмом и тегами
     */
    public static function delLink($id)
    {
        $db = Db::getConnection();
        $sql = 'DELETE FROM distribution WHERE id_film = :id';
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->execute();
    }

    /*
     * Удаление фильма
     */
    public static function delFilm($id)
    {
        $db = Db::getConnection();

        $sql = 'DELETE distribution, films 
                FROM distribution 
                LEFT JOIN films ON distribution.id_film = films.id 
                WHERE distribution.id_film = :id';
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->execute();
    }

    /*
     * удаление тега с главной страницы
     */
    public static function delTag($idFilm, $idTag)
    {
        $db = Db::getConnection();
        $sql = 'DELETE FROM distribution                 
                WHERE distribution.id_film = :id_film AND distribution.id_tag = :id_tag';
        $result = $db->prepare($sql);
        $result->bindParam(':id_film', $idFilm, PDO::PARAM_INT);
        $result->bindParam(':id_tag', $idTag, PDO::PARAM_INT);
        $result->execute();
    }
}