<?php

class FilmController
{
    public function actionAdd()
    {
        $titleFilm = false;
        $yearFilm = false;
        $tagsFilm = false;

        if(isset($_POST['submit'])) {
            $titleFilm = $_POST['title'];
            $yearFilm = $_POST['year'];
            $tagsFilm = $_POST['tags'];

            //преобразуем строку в массив с отдельными значениями, убираем повторения в массиве, убираем пробел и запятую
            $tagsFilm = array_unique(explode(" ",str_replace(",","", $tagsFilm)));
            //добавление фильма
            $id = Films::addFilm($titleFilm, $yearFilm);
            //добавление тегов
            Films::addTags($tagsFilm);
            //получение id тегов, добавленных в запросе
            $idTags = Films::getIdTagCreateFilm($tagsFilm);
            //добавление связей между фильмом и тегами
            Films::addLink($id, $idTags);
            header('Location:/');
        }
        require_once (ROOT . '/views/film/add.php');
        return true;
    }

    /*
     * изменение фильма
     */
    public function actionEdit($id)
    {
        $titleFilm = false;
        $yearFilm = false;
        $tagsFilm = false;
        $filmItem = Films::getFilmByID($id);

        if(isset($_POST['submit'])) {
            $titleFilm = $_POST['title'];
            $yearFilm = $_POST['year'];
            $tagsFilm = $_POST['tags'];

            //преобразуем строку в массив с отдельными значениями, убираем повторения в массиве, убираем пробел и запятую
            $tagsFilm = array_unique(explode(" ",str_replace(",","", $tagsFilm)));
            //обновление фильма
            Films::updateFilm($titleFilm, $yearFilm, $id);
            //добавление, изменение тегов
            Films::addTags($tagsFilm);
            //получение id тегов, добавленных в запросе
            $idTags = Films::getIdTagCreateFilm($tagsFilm);
            //удаление связи между фильмом и тегами
            Films::delLink($id);
            //добавление связей между фильмом и тегами
            Films::addLink($id, $idTags);
            header('Location:/');
        }

        require_once (ROOT . '/views/film/edit.php');
        return true;
    }

    /*
     * удаление фильма
     */
    public function actionDel($id)
    {
        Films::delFilm($id);
        header('Location:/');
        return true;
    }

    /*
     * удаление тега с главной страницы
     */
    public function actionDelTag($idFilm, $idTag)
    {
        Films::delTag($idFilm, $idTag);
        header('Location:/');
        return true;
    }
}