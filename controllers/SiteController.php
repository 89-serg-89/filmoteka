<?php

class SiteController
{
    public function actionIndex()
    {
        $films = Films::getFilmsList();

        require_once (ROOT . '/views/site/index.php');
        return true;
    }
}